#include "debuglogger.h"

DebugLogger::DebugLogger()
{
    m_debugLogger = new QOpenGLDebugLogger(this);
    if (m_debugLogger->initialize())
    {
        qDebug() << "GL_DEBUG Debug Logger" << m_debugLogger << "\n";
        connect(m_debugLogger, &QOpenGLDebugLogger::messageLogged, this, &DebugLogger::messageLogged);
        m_debugLogger->startLogging();
    }
}

DebugLogger::~DebugLogger()
{
    if (m_debugLogger != nullptr)
        delete m_debugLogger;
}

void DebugLogger::messageLogged(const QOpenGLDebugMessage &msg)
{
    QString error;

    // Format based on severity
    switch (msg.severity())
    {
    case QOpenGLDebugMessage::NotificationSeverity:
        error += "--";
        break;
    case QOpenGLDebugMessage::HighSeverity:
        error += "!!";
        break;
    case QOpenGLDebugMessage::MediumSeverity:
        error += "!~";
        break;
    case QOpenGLDebugMessage::LowSeverity:
        error += "~~";
        break;
    }

    error += " (";

    // Format based on source
#define CASE(c) case QOpenGLDebugMessage::c: error += #c; break
    switch (msg.source())
    {
        CASE(APISource);
        CASE(WindowSystemSource);
        CASE(ShaderCompilerSource);
        CASE(ThirdPartySource);
        CASE(ApplicationSource);
        CASE(OtherSource);
        CASE(InvalidSource);
    }
#undef CASE

    error += " : ";

    // Format based on type
#define CASE(c) case QOpenGLDebugMessage::c: error += #c; break
    switch (msg.type())
    {
        CASE(ErrorType);
        CASE(DeprecatedBehaviorType);
        CASE(UndefinedBehaviorType);
        CASE(PortabilityType);
        CASE(PerformanceType);
        CASE(OtherType);
        CASE(MarkerType);
        CASE(GroupPushType);
        CASE(GroupPopType);
    }
#undef CASE

    error += ")";
    qDebug() << qPrintable(error) << "\n" << qPrintable(msg.message()) << "\n";
}
