#include "scene.h"
#include <iostream>

Scene::Scene(QOpenGLFunctions_4_4_Core *gl) : m_gl(gl)
{

}

bool Scene::loadModel(QString fileName)
{
    if (m_model != nullptr)
        delete m_model;

    m_model = new tinygltf::Model();

    tinygltf::TinyGLTF loader;
    std::string error;
    std::string warning;

    // load glTF model from file and check errors
    bool result = loader.LoadASCIIFromFile(m_model, &error, &warning, fileName.toUtf8().constData());
    if (!error.empty())
    {
        std::cout << "Error: " << error.c_str() << std::endl;
        return false;
    }

    if (!warning.empty())
    {
        std::cout << "Warning: " << warning.c_str() << std::endl;
        return false;
    }

    if (!result)
    {
        std::cout << "Failed to parse glTF." << std::endl;
        return false;
    }

    return true;
}

void Scene::loadBufferViews()
{
    // load data into VBOs
    for (unsigned int i = 0; i < m_model->bufferViews.size(); i++)
    {
        const tinygltf::BufferView &bufferView = m_model->bufferViews[i];
        const tinygltf::Buffer &buffer = m_model->buffers[static_cast<unsigned long long>(bufferView.buffer)];

        GLuint vbo;
        m_gl->glGenBuffers(1, &vbo);
        m_vbos[i] = vbo;

        m_gl->glBindBuffer(static_cast<GLuint>(bufferView.target), m_vbos[i]);
        m_gl->glBufferData(static_cast<GLuint>(bufferView.target), static_cast<long long>(bufferView.byteLength), &buffer.data.at(0) + bufferView.byteOffset, GL_STATIC_DRAW);
        m_gl->glBindBuffer(static_cast<GLuint>(bufferView.target), 0);
    }
}

void Scene::loadLights()
{
    auto ext = m_model->extensions.find("KHR_light_cmn");
    if (ext == m_model->extensions.end())
        return;

    tinygltf::Value &extension = ext->second;
    const tinygltf::Value &lights = extension.Get("lights");

    for (unsigned int i = 0; i < lights.Size(); i++)
    {
        const tinygltf::Value &value = lights.Get(static_cast<int>(i));
        const tinygltf::Value &color = value.Get("Color");
        Light light;

        for (int j = 0; j < 3; j++)
        {
            light.color[i] = static_cast<float>(color.Get(j).Get<double>());
        }

        m_lights.push_back(light);
    }
}

void Scene::loadMaterials()
{
    if (m_materials != nullptr)
        delete [] m_materials;

    m_materials = new Material[m_model->materials.size()];

    for (unsigned int i = 0; i < m_model->materials.size(); i++)
    {
        tinygltf::Material &mat = m_model->materials[i];
        Material material;

        auto blinnPhongQuery = mat.extensions.find("KHR_materials_cmnBlinnPhong");
        if (blinnPhongQuery != mat.extensions.end())
        {
            tinygltf::Value &blinnPhongData = blinnPhongQuery->second;

            if (blinnPhongData.Has("diffuseFactor"))
            {
                for (int i = 0; i < 4; i++)
                {
                    material.diffuseFactor[i] = static_cast<float>(blinnPhongData.Get("diffuseFactor").Get(i).Get<double>());
                }
            }

            if (blinnPhongData.Has("diffuseTexture"))
            {
                material.diffuseTexture = blinnPhongData.Get("diffuseTexture").Get("index").Get<int>();
            }

            if (blinnPhongData.Has("specularFactor"))
            {
                for (int i = 0; i < 3; i++)
                {
                    material.specularFactor[i] = static_cast<float>(blinnPhongData.Get("specularFactor").Get(i).Get<double>());
                }
            }

            if (blinnPhongData.Has("specularTexture"))
            {
                material.specularTexture = blinnPhongData.Get("specularTexture").Get("index").Get<int>();
            }

            if (blinnPhongData.Has("shininessFactor"))
            {
                material.shininessFactor = static_cast<float>(blinnPhongData.Get("shininessFactor").Get<double>());
            }

            if (blinnPhongData.Has("shininessTexture"))
            {
                material.shininessTexture = blinnPhongData.Get("shininessTexture").Get("index").Get<int>();
            }
        }

        m_materials[i] = material;
    }

    m_gl->glGenBuffers(1, &m_ubo);
    m_gl->glBindBuffer(GL_UNIFORM_BUFFER, m_ubo);
    m_gl->glBufferData(GL_UNIFORM_BUFFER, static_cast<GLsizeiptr>(m_model->materials.size() * sizeof(Material)), m_materials, GL_STATIC_DRAW);
    m_gl->glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void Scene::loadTextures()
{
    GLuint textureId;
    m_gl->glGenTextures(1, &textureId);
    m_gl->glActiveTexture(GL_TEXTURE3);
    m_gl->glBindTexture(GL_TEXTURE_2D_ARRAY, textureId);
    m_gl->glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    m_gl->glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    m_gl->glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    m_gl->glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    for (unsigned int i = 0; i < m_model->textures.size(); i++)
    {
        tinygltf::Texture &texture = m_model->textures[i];
        tinygltf::Image &image = m_model->images[static_cast<unsigned long long>(texture.source)];

        if (i == 0)
        {
            m_gl->glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_RGBA8, image.width, image.height, static_cast<int>(m_model->textures.size()));
        }

        GLenum format = GL_RGBA;
        if (image.component == 1)
        {
            format = GL_RED;
        }
        else if (image.component == 2)
        {
            format = GL_RG;
        }
        else if (image.component == 3)
        {
            format = GL_RGB;
        }

        GLenum type = GL_UNSIGNED_BYTE;
        if (image.bits == 16)
        {
            type = GL_UNSIGNED_SHORT;
        }

        m_gl->glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, static_cast<int>(i), image.width, image.height, 1, format, type, image.image.data());
    }
}

void Scene::loadAnimations()
{
    for (unsigned int i = 0; i < m_model->animations.size(); i++)
    {
        tinygltf::Animation &animation = m_model->animations[i];
        m_keyFrames[i] = std::map<unsigned int, std::vector<float>>();
        m_rotations[i] = std::map<unsigned int, std::vector<QVector4D>>();
        m_translations[i] = std::map<unsigned int, std::vector<QVector3D>>();
        m_scales[i] = std::map<unsigned int, std::vector<QVector3D>>();

        for (unsigned int j = 0; j < animation.channels.size(); j++)
        {
            tinygltf::AnimationChannel &channel = animation.channels[j];
            tinygltf::AnimationSampler &sampler = animation.samplers[static_cast<unsigned long long>(channel.sampler)];
            loadKeyFrames(i, j, sampler);
            loadTransformations(i, j, channel, sampler);
        }
    }
}

void Scene::loadKeyFrames(unsigned int animationIndex, unsigned int channelIndex, tinygltf::AnimationSampler &sampler)
{
    tinygltf::Accessor &input = m_model->accessors[static_cast<unsigned long long>(sampler.input)];
    float *data = loadAnimationBuffer(input, m_model->bufferViews[static_cast<unsigned long long>(input.bufferView)], 4 * input.count);

    std::vector<float> vector;
    for (unsigned int i = 0; i < input.count; i++)
    {
        vector.push_back(data[i]);
    }
    m_keyFrames[animationIndex][channelIndex] = vector;
}

void Scene::loadTransformations(unsigned int animationIndex, unsigned int channelIndex, tinygltf::AnimationChannel &channel, tinygltf::AnimationSampler &sampler)
{
    tinygltf::Accessor &output = m_model->accessors[static_cast<unsigned long long>(sampler.output)];
    unsigned int entrySize = 1;
    if (output.type == TINYGLTF_TYPE_VEC3 || output.type == TINYGLTF_TYPE_VEC4)
    {
        entrySize = static_cast<unsigned int>(output.type);
    }

    float *data = loadAnimationBuffer(output, m_model->bufferViews[static_cast<unsigned long long>(output.bufferView)], entrySize * output.count);
    if (channel.target_path.compare("translation") == 0)
    {
        for (unsigned int i = 0; i < output.count; i++)
        {
            QVector3D vector(data[entrySize * i], data[entrySize * i + 1], data[entrySize * i + 2]);
            m_translations[animationIndex][channelIndex].push_back(vector);
        }
    }
    else if (channel.target_path.compare("rotation") == 0)
    {
        for (unsigned int i = 0; i < output.count; i++)
        {
            QVector4D vector(data[entrySize * i], data[entrySize * i + 1], data[entrySize * i + 2], data[entrySize * i + 3]);
            m_rotations[animationIndex][channelIndex].push_back(vector);
        }
    }
    else if (channel.target_path.compare("scale") == 0)
    {
        for (unsigned int i = 0; i < output.count; i++)
        {
            QVector3D vector(data[entrySize * i], data[entrySize * i + 1], data[entrySize * i + 2]);
            m_scales[animationIndex][channelIndex].push_back(vector);
        }
    }
}

float* Scene::loadAnimationBuffer(tinygltf::Accessor &accessor, tinygltf::BufferView &bufferView, size_t dataCount)
{
    tinygltf::Buffer &buffer = m_model->buffers[static_cast<unsigned long long>(bufferView.buffer)];
    std::vector<unsigned char> *data = new std::vector<unsigned char>();

    for (size_t i = 0, pos = accessor.byteOffset + bufferView.byteOffset; i < dataCount; i++, pos += bufferView.byteStride)
    {
        for (unsigned int j = 0; j < bufferView.byteLength; j++)
        {
            data->push_back(buffer.data[pos + j]);
        }
    }

    return reinterpret_cast<float*>(data->data());
}

void Scene::loadMesh(tinygltf::Mesh &mesh)
{
    m_vaos[mesh.name] = std::vector<GLuint>();
    for (unsigned int i = 0; i < mesh.primitives.size(); i++)
    {
        GLuint vao;
        m_gl->glGenVertexArrays(1, &vao);
        m_gl->glBindVertexArray(vao);
        m_vaos[mesh.name].push_back(vao);

        tinygltf::Primitive &primitive = mesh.primitives[i];
        for (auto &attribute : primitive.attributes)
        {
            // find and bind the current buffer
            const tinygltf::Accessor accessor = m_model->accessors[static_cast<unsigned long long>(attribute.second)];
            int byteStride = accessor.ByteStride(m_model->bufferViews[static_cast<unsigned long long>(accessor.bufferView)]);
            m_gl->glBindBuffer(GL_ARRAY_BUFFER, m_vbos[static_cast<unsigned int>(accessor.bufferView)]);

            // get the buffer size
            int size = 1;
            if (accessor.type != TINYGLTF_TYPE_SCALAR)
            {
                size = accessor.type;
            }

            // get the data type (pos, normal, color)
            int vaa = -1;
            if (attribute.first.compare("POSITION") == 0)
                vaa = 0;
            if (attribute.first.compare("NORMAL") == 0)
                vaa = 1;
            if (attribute.first.compare("TEXCOORD_0") == 0)
                vaa = 2;
            if (vaa > -1)
            {
                // enable correct array and set it's parameters
                m_gl->glEnableVertexAttribArray(static_cast<unsigned int>(vaa));
                m_gl->glVertexAttribPointer(static_cast<unsigned int>(vaa), size, static_cast<unsigned int>(accessor.componentType),
                                      accessor.normalized ? GL_TRUE : GL_FALSE, byteStride, reinterpret_cast<GLvoid*>(accessor.byteOffset));
            }
        }
    }

    m_gl->glBindVertexArray(0);
}

void Scene::initialize()
{
    m_gl->glBindVertexArray(0);

    loadBufferViews();
    loadLights();
    loadMaterials();
    loadTextures();
    loadAnimations();

    for (unsigned int i = 0; i < m_model->meshes.size(); i++)
    {
        tinygltf::Mesh &mesh = m_model->meshes[i];
        loadMesh(mesh);
    }
}

template<> std::vector<double> Scene::computeTransformation(std::vector<QVector3D> &transformations, float alpha, unsigned int keyFrameIndex)
{
    QVector3D v0 = transformations[keyFrameIndex];
    QVector3D v1 = transformations[keyFrameIndex + 1];
    QVector3D v = (1 - alpha) * v0 + alpha * v1;

    std::vector<double> vector;
    vector.push_back(static_cast<double>(v.x()));
    vector.push_back(static_cast<double>(v.y()));
    vector.push_back(static_cast<double>(v.z()));
    return vector;
}

template<> std::vector<double> Scene::computeTransformation(std::vector<QVector4D> &transformations, float alpha, unsigned int keyFrameIndex)
{
    QVector4D v0 = transformations[keyFrameIndex];
    QVector4D v1 = transformations[keyFrameIndex + 1];
    QVector4D v = (1 - alpha) * v0 + alpha * v1;

    std::vector<double> vector;
    vector.push_back(static_cast<double>(v.x()));
    vector.push_back(static_cast<double>(v.y()));
    vector.push_back(static_cast<double>(v.z()));
    vector.push_back(static_cast<double>(v.w()));
    return vector;
}

bool Scene::animate(float time)
{
    bool everythingAnimated = true;

    for (unsigned int i = 0; i < m_model->animations.size(); i++)
    {
        tinygltf::Animation &animation = m_model->animations[i];
        for (unsigned int j = 0; j < animation.channels.size(); j++)
        {
            tinygltf::AnimationChannel &channel = animation.channels[j];
            std::vector<float> &keyFrames = m_keyFrames[i][j];

            unsigned int keyFrameIndex;
            std::pair<float, float> keyTimes = getKeyFrames(keyFrames, &keyFrameIndex, time);
            float alpha = std::min(1.0F, std::max(0.0F, (time - keyTimes.first) / (keyTimes.second - keyTimes.first)));
            tinygltf::Node &node = m_model->nodes[static_cast<unsigned long long>(channel.target_node)];

            if (channel.target_path.compare("translation") == 0)
            {
                node.translation = computeTransformation(m_translations[i][j], alpha, keyFrameIndex);
            }
            else if (channel.target_path.compare("rotation") == 0)
            {
                node.rotation = computeTransformation(m_rotations[i][j], alpha, keyFrameIndex);
            }
            else if (channel.target_path.compare("scale") == 0)
            {
                node.scale = computeTransformation(m_scales[i][j], alpha, keyFrameIndex);
            }

            if (time > keyFrames[keyFrames.size() - 1])
                continue;

            everythingAnimated = false;
        }
    }

    return everythingAnimated;
}

std::pair<float, float> Scene::getKeyFrames(std::vector<float> &keyFrames, unsigned int *keyFrameIndex, float time)
{
    unsigned int keyFrame = 0;
    while (keyFrame < keyFrames.size() - 2 && keyFrames[keyFrame + 1] < time)
    {
        keyFrame++;
    }
    *keyFrameIndex = keyFrame;
    return std::pair<float, float>(keyFrames[keyFrame], keyFrames[keyFrame + 1]);
}

void Scene::draw(QOpenGLShaderProgram *shader)
{
    tinygltf::Scene &scene = m_model->scenes[static_cast<unsigned long long>(m_model->defaultScene)];
    for (unsigned int i = 0; i < scene.nodes.size(); i++)
    {
        m_ms.clear();
        m_ms.push_back(QMatrix4x4());

        const unsigned int nodeIndex = static_cast<unsigned int>(scene.nodes[i]);
        drawModelNodes(m_model->nodes[nodeIndex], shader);
    }
}

void Scene::drawModelNodes(tinygltf::Node &node, QOpenGLShaderProgram *shader)
{
    QMatrix4x4 m = m_ms.at(m_ms.size() - 1);
    if (node.translation.size() == 3)
    {
        double *translation = node.translation.data();
        m.translate(static_cast<float>(translation[0]), static_cast<float>(translation[1]),
                static_cast<float>(translation[2]));
    }
    if (node.rotation.size() == 4)
    {
        double *rotation = node.rotation.data();
        m.rotate(QQuaternion(static_cast<float>(rotation[3]), static_cast<float>(rotation[0]),
                static_cast<float>(rotation[1]), static_cast<float>(rotation[2])).normalized());
    }
    if (node.scale.size() == 3)
    {
        double *scale = node.scale.data();
        m.scale(static_cast<float>(scale[0]), static_cast<float>(scale[1]),
                static_cast<float>(scale[2]));
    }
    if (node.matrix.size() == 16)
    {
        double *mat = node.matrix.data();
        m = QMatrix4x4(static_cast<float>(mat[0]), static_cast<float>(mat[1]), static_cast<float>(mat[2]), static_cast<float>(mat[3]),
                static_cast<float>(mat[4]), static_cast<float>(mat[5]), static_cast<float>(mat[6]), static_cast<float>(mat[7]),
                static_cast<float>(mat[8]), static_cast<float>(mat[9]), static_cast<float>(mat[10]), static_cast<float>(mat[11]),
                static_cast<float>(mat[12]), static_cast<float>(mat[13]), static_cast<float>(mat[14]), static_cast<float>(mat[15])) * m;
    }

    m_ms.push_back(m);

    if (node.mesh >= 0)
    {
        drawMesh(m_model->meshes[static_cast<unsigned long long>(node.mesh)], shader);
    }

    for (unsigned int i = 0; i < node.children.size(); i++)
    {
        drawModelNodes(m_model->nodes[static_cast<unsigned int>(node.children[i])], shader);
    }

    m_ms.pop_back();
}

void Scene::drawMesh(tinygltf::Mesh &mesh, QOpenGLShaderProgram *shader)
{
    for (unsigned int i = 0; i < mesh.primitives.size(); i++)
    {
        tinygltf::Primitive primitive = mesh.primitives[i];
        tinygltf::Accessor accessor = m_model->accessors[static_cast<unsigned long long>(primitive.indices)];

        m_gl->glBindVertexArray(m_vaos[mesh.name].at(i));

        const tinygltf::BufferView &bufferView = m_model->bufferViews[static_cast<unsigned long long>(accessor.bufferView)];
        m_gl->glBindBuffer(static_cast<GLuint>(bufferView.target), m_vbos[static_cast<unsigned int>(accessor.bufferView)]);

        QMatrix4x4 &m = m_ms.at(m_ms.size() - 1);
        shader->setUniformValue("m", m);
        shader->setUniformValue("n", m.inverted().transposed());
        shader->setUniformValue("materialId", primitive.material);

        m_gl->glDrawElements(static_cast<GLenum>(primitive.mode), static_cast<int>(accessor.count), static_cast<GLenum>(accessor.componentType),
                       reinterpret_cast<GLvoid*>(accessor.byteOffset));

        m_gl->glBindVertexArray(0);
    }
}

void Scene::bindMaterials(QOpenGLShaderProgram *shader)
{
    shader->setUniformValue("numberOfMaterials", static_cast<int>(m_model->materials.size()));
    m_gl->glBindBufferBase(GL_UNIFORM_BUFFER, 4, m_ubo);
}
