#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets>
#include <QMainWindow>
#include "helloglwidget.h"
#include "timinggraph.h"

// main window for Qt
class MainWindow : public QMainWindow
{
    // necessary macro to enable Qt functionalities like signals, slots, etc.
    Q_OBJECT

public:
    // derived constructor
    MainWindow(QWidget *parent = 0);
    // derived destructor (default so we don't have to implement it)
    virtual ~MainWindow() = default;

public slots:
    // shows an OpenFileDialog and loads the model if the user selected one
    void openModel();
    // shows the about window with information about OpenGL, etc.
    void showAboutBox();
    // updates the status bar text with the current camera position and view direction
    void updateStatusBarText(QVector3D camera, QVector3D direction);

private:
    // the HelloGLWidget
    HelloGLWidget *m_glWidget;
    // the status bar
    QStatusBar *m_statusBar;
    // the timing graph
    TimingGraph *m_timingGraph;
};

#endif // MAINWINDOW_H
