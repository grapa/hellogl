#include "timinggraph.h"
#include <iostream>

TimingGraph::TimingGraph()
{
    m_chart = new QChart();

    m_xAxis = new QValueAxis();
    m_xAxis->setRange(0, 100);
    m_xAxis->setTickCount(5);
    m_xAxis->setTitleText("Frame Number");
    m_xAxis->setTitleVisible();
    m_chart->addAxis(m_xAxis, Qt::AlignBottom);

    m_yAxis = new QValueAxis();
    m_yAxis->setRange(0, 200000);
    m_yAxis->setTitleText("Time (ns)");
    m_yAxis->setTitleVisible();
    m_chart->addAxis(m_yAxis, Qt::AlignLeft);
}

QChart* TimingGraph::getChart()
{
    return m_chart;
}

void TimingGraph::pushData(int frame, std::vector<uint64_t> &data)
{
    if (frame == 0)
        return;

    if (frame > m_xAxis->max())
    {
        m_xAxis->setRange(frame - 100 < 0 ? 0 : frame - 100, frame);
    }

    for (unsigned int i = 0; i < data.size(); i++)
    {
        if (i >= m_series.size())
        {
            QLineSeries *series = new QLineSeries();

            m_series.push_back(series);
            m_chart->addSeries(series);
            m_chart->setAxisX(m_xAxis, series);
            m_chart->setAxisY(m_yAxis, series);

            if (i == 0)
            {
                series->setName("Geometry Pass");
            }
            else if (i == 1)
            {
                series->setName("Deferred Shading Pass");
            }
        }

        QLineSeries *series = m_series.at(i);
        series->append(frame, static_cast<double>(data.at(i)));
        while (series->points().size() > 101)
        {
            series->remove(0);
        }
    }

    double maxValue = 0;
    for (unsigned int i = 0; i < m_series.size(); i++)
    {
        QLineSeries *series = m_series.at(i);
        for (int i = 0; i < series->points().size(); i++)
        {
            if (series->points().at(i).y() > maxValue)
            {
                maxValue = series->points().at(i).y();
            }
        }
    }

    double axisMax = m_yAxis->max();
    if (maxValue > axisMax)
    {
        m_yAxis->setMax(maxValue + 10000.0);
    }
    else if (maxValue < axisMax - 50000)
    {
        m_yAxis->setMax(maxValue + 10000.0);
    }
}
