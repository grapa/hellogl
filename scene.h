#ifndef SCENE_H
#define SCENE_H

#include "tinygltf/json.hpp"
#include "tinygltf/tiny_gltf.h"
#include <QOpenGLFunctions_4_4_Core>
#include <QOpenGLShaderProgram>

typedef int ShaderOutputMode;
#define SHADER_OUTPUT_MODE_DEFAULT 0
#define SHADER_OUTPUT_MODE_NORMAL 1
#define SHADER_OUTPUT_MODE_TEXTURE_COORD 2
#define SHADER_OUTPUT_MODE_MATERIAL_ID 3
#define SHADER_OUTPUT_MODE_MATERIAL_COLOR 4
#define SHADER_OUTPUT_MODE_RECONSTRUCTED_POSITION 5

// padded to 64 byte size
struct Material
{
    float diffuseFactor[4] = {0, 1, 0, 1};
    int diffuseTexture = -1;
    float shininessFactor = 10;
    int shininessTexture = -1;
    int specularTexture = -1;
    float specularFactor[3] = {1, 0, 1};
    char padding[4];
};

struct Light
{
    float color[3];
};

class Scene
{
public:
    Scene(QOpenGLFunctions_4_4_Core *gl);
    virtual ~Scene() = default;

    bool loadModel(QString fileName);
    void initialize();
    bool animate(float time);
    void draw(QOpenGLShaderProgram *shader);
    void bindMaterials(QOpenGLShaderProgram *shader);

private:
    void loadBufferViews();
    void loadLights();
    void loadMaterials();
    void loadTextures();
    void loadAnimations();
    void loadKeyFrames(unsigned int animationIndex, unsigned int channelIndex,
                       tinygltf::AnimationSampler &sampler);
    void loadTransformations(unsigned int animationIndex, unsigned int channelIndex,
                             tinygltf::AnimationChannel &channel, tinygltf::AnimationSampler &sampler);
    float* loadAnimationBuffer(tinygltf::Accessor &accessor, tinygltf::BufferView &bufferView, size_t dataCount);
    void loadMesh(tinygltf::Mesh &mesh);
    void animateNodes();
    std::pair<float, float> getKeyFrames(std::vector<float> &keyFrames, unsigned int *keyFrameIndex, float time);
    template<typename T> std::vector<double> computeTransformation(std::vector<T> &transformations, float alpha, unsigned int keyFrameIndex);
    void drawModelNodes(tinygltf::Node &node, QOpenGLShaderProgram *shader);
    void drawMesh(tinygltf::Mesh &mesh, QOpenGLShaderProgram *shader);
    void measureTime(unsigned int index);
    void reportTimings();

    std::map<unsigned int, std::map<unsigned int, std::vector<float>>> m_keyFrames;
    std::map<unsigned int, std::map<unsigned int, std::vector<QVector4D>>> m_rotations;
    std::map<unsigned int, std::map<unsigned int, std::vector<QVector3D>>> m_translations;
    std::map<unsigned int, std::map<unsigned int, std::vector<QVector3D>>> m_scales;

    QOpenGLFunctions_4_4_Core *m_gl;

    std::map<unsigned int, GLuint> m_vbos;
    std::map<std::string, std::vector<GLuint>> m_vaos;
    GLuint m_ubo;
    std::vector<QMatrix4x4> m_ms;

    QVector3D *m_lightPos;
    float *m_lightIntensity;

    tinygltf::Model *m_model = nullptr;
    Material *m_materials = nullptr;

    ShaderOutputMode *m_outputMode;

    GLuint m_lightVao;
    GLuint m_lightPosVbo;
    GLuint m_lightColorVbo;
    std::vector<Light> m_lights;
};

#endif // SCENE_H
