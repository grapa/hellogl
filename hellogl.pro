#-------------------------------------------------
#
# Project created by QtCreator 2019-05-09T11:56:03
#
#-------------------------------------------------

QT       += core gui opengl charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = hellogl
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        debuglogger.cpp \
        errorchecker.cpp \
        helloglwidget.cpp \
        main.cpp \
        mainwindow.cpp \
        scene.cpp \
        timinggraph.cpp

HEADERS += \
        debuglogger.h \
        errorchecker.h \
        helloglwidget.h \
        mainwindow.h \
        scene.h \
        timinggraph.h \
        tinygltf/json.hpp \
        tinygltf/stb_image.h \
        tinygltf/stb_image_write.h \
        tinygltf/tiny_gltf.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    deferred.frag \
    deferred.vert \
    fullimage.frag \
    fullimage.vert \
    geometry.frag \
    geometry.vert

RESOURCES += \
    hellogl.qrc
