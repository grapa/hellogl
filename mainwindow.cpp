#include "mainwindow.h"
#include "helloglwidget.h"
#include <QtWidgets>
#include <iostream>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    // set window title
    setWindowTitle("Hello GL");

    // add timing graph
    m_timingGraph = new TimingGraph();
    QChartView *chartView = new QChartView(m_timingGraph->getChart());
    chartView->setRenderHint(QPainter::Antialiasing);
    QDockWidget *dockWidget = new QDockWidget("Timing Graph");
    dockWidget->setWidget(chartView);
    addDockWidget(Qt::DockWidgetArea::BottomDockWidgetArea, dockWidget);

    // add HelloGLWidget
    m_glWidget = new HelloGLWidget(m_timingGraph);
    setCentralWidget(m_glWidget);

    // add menu bar
    QMenuBar *menuBar = new QMenuBar();

    // add file menu with open model and close application
    QMenu *fileMenu = menuBar->addMenu("&File");
    QAction *openAction = fileMenu->addAction("Open");
    fileMenu->addSeparator();
    QAction *exitAction = fileMenu->addAction("E&xit");
    // set shortcut for closing application
    exitAction->setShortcut(QKeySequence("Ctrl+q"));

    // shader mode menu
    QMenu *shaderModeMenu = menuBar->addMenu("Shader Mode");

    QAction *defaultAction = shaderModeMenu->addAction("Default");
    defaultAction->setCheckable(true);

    QAction *normalAction = shaderModeMenu->addAction("Normals");
    normalAction->setCheckable(true);

    QAction *textureCoordAction = shaderModeMenu->addAction("Texture Coordinate");
    textureCoordAction->setCheckable(true);

    QAction *materialIdAction = shaderModeMenu->addAction("Material ID");
    materialIdAction->setCheckable(true);

    QAction *materialColorAction = shaderModeMenu->addAction("Material Diffuse Color");
    materialColorAction->setCheckable(true);

    QAction *reconstructedPositionAction = shaderModeMenu->addAction("Reconstructed Position");
    reconstructedPositionAction->setCheckable(true);

    QActionGroup *shaderModeGroup = new QActionGroup(shaderModeMenu);
    shaderModeGroup->addAction(defaultAction);
    shaderModeGroup->addAction(normalAction);
    shaderModeGroup->addAction(textureCoordAction);
    shaderModeGroup->addAction(materialIdAction);
    shaderModeGroup->addAction(materialColorAction);
    shaderModeGroup->addAction(reconstructedPositionAction);
    defaultAction->setChecked(true);

    // add help menu and about action
    QMenu *helpMenu = menuBar->addMenu("Help");
    QAction *aboutAction = helpMenu->addAction("About");

    // add menu bar to window
    setMenuBar(menuBar);

    // add tool bar with shading actions and camera reset button
    QToolBar *toolBar = new QToolBar();

    // camera reset button
    QAction *resetCameraAction = toolBar->addAction("Reset Camera");
    resetCameraAction->setIcon(QIcon(":/img/cam_home.png"));

    addToolBar(toolBar);

    // full image
    toolBar = new QToolBar();
    QAction *normalModeAction = toolBar->addAction("Normal");
    normalModeAction->setCheckable(true);

    QAction *fullImageAction = toolBar->addAction("Full Image");
    fullImageAction->setCheckable(true);

    QActionGroup *fullImageGroup = new QActionGroup(toolBar);
    fullImageGroup->addAction(normalModeAction);
    fullImageGroup->addAction(fullImageAction);
    normalModeAction->setChecked(true);

    addToolBar(toolBar);

    // pause/play animation
    toolBar = new QToolBar();
    QAction *pauseAction = toolBar->addAction("Pause");
    pauseAction->setCheckable(true);

    QAction *playAction = toolBar->addAction("Play");
    playAction->setCheckable(true);

    QActionGroup *animationGroup = new QActionGroup(toolBar);
    animationGroup->addAction(pauseAction);
    animationGroup->addAction(playAction);
    playAction->setChecked(true);

    addToolBar(toolBar);

    // add tool bar for light position and intensity
    toolBar = new QToolBar();

    // x position
    QLabel *label = new QLabel("Light Pos X:");
    toolBar->addWidget(label);

    QSpinBox *lpX = new QSpinBox();
    lpX->setRange(-10, 10);
    lpX->setValue(-2);
    toolBar->addWidget(lpX);

    // y position
    label = new QLabel(" Y:");
    toolBar->addWidget(label);

    QSpinBox *lpY = new QSpinBox();
    lpY->setRange(-10, 10);
    lpY->setValue(2);
    toolBar->addWidget(lpY);

    // z position
    label = new QLabel(" Z:");
    toolBar->addWidget(label);

    QSpinBox *lpZ = new QSpinBox();
    lpZ->setRange(-10, 10);
    lpZ->setValue(-1);
    toolBar->addWidget(lpZ);

    // intensity
    label = new QLabel(" I:");
    toolBar->addWidget(label);

    QSpinBox *lI = new QSpinBox();
    lI->setRange(0, 10);
    lI->setValue(5);
    toolBar->addWidget(lI);

    addToolBar(toolBar);

    // add status bar to display camera position and view direction
    m_statusBar = new QStatusBar();
    setStatusBar(m_statusBar);

    // connect all actions
    connect(openAction, &QAction::triggered, this, &MainWindow::openModel);
    connect(exitAction, &QAction::triggered, this, &MainWindow::close);
    connect(aboutAction, &QAction::triggered, this, &MainWindow::showAboutBox);
    connect(resetCameraAction, &QAction::triggered, m_glWidget, &HelloGLWidget::resetCamera);
    connect(m_glWidget, &HelloGLWidget::cameraChanged, this, &MainWindow::updateStatusBarText);
    connect(lpX, QOverload<int>::of(&QSpinBox::valueChanged), m_glWidget, &HelloGLWidget::setLightX);
    connect(lpY, QOverload<int>::of(&QSpinBox::valueChanged), m_glWidget, &HelloGLWidget::setLightY);
    connect(lpZ, QOverload<int>::of(&QSpinBox::valueChanged), m_glWidget, &HelloGLWidget::setLightZ);
    connect(lI, QOverload<int>::of(&QSpinBox::valueChanged), m_glWidget, &HelloGLWidget::setLightIntensity);
    connect(normalModeAction, &QAction::triggered, m_glWidget, &HelloGLWidget::deferredShaderMode);
    connect(fullImageAction, &QAction::triggered, m_glWidget, &HelloGLWidget::fullImageShaderMode);
    connect(pauseAction, &QAction::triggered, m_glWidget, &HelloGLWidget::pauseAnimation);
    connect(playAction, &QAction::triggered, m_glWidget, &HelloGLWidget::playAnimation);
    connect(defaultAction, &QAction::triggered, m_glWidget, &HelloGLWidget::defaultShaderMode);
    connect(normalAction, &QAction::triggered, m_glWidget, &HelloGLWidget::normalShaderMode);
    connect(textureCoordAction, &QAction::triggered, m_glWidget, &HelloGLWidget::textureCoordShaderMode);
    connect(materialIdAction, &QAction::triggered, m_glWidget, &HelloGLWidget::materialIdShaderMode);
    connect(materialColorAction, &QAction::triggered, m_glWidget, &HelloGLWidget::materialColorShaderMode);
    connect(reconstructedPositionAction, &QAction::triggered, m_glWidget, &HelloGLWidget::reconstructedPositionShaderMode);

    // resize window
    resize(1280, 720);

    // write initial camera position and view direction into status bar
    updateStatusBarText(m_glWidget->getCameraPosition(), m_glWidget->getCameraViewDirection());
}

void MainWindow::openModel()
{
    // show file dialog
    QString fileName = QFileDialog::getOpenFileName(this, "Open Model", QDir::currentPath(), "Model Files (*.gltf)");
    // check if user selected a file
    if (fileName.isNull())
        return;

    // load model
    m_glWidget->loadModel(fileName);
}

void MainWindow::showAboutBox()
{
    std::string text = "Written by Simon Grossmann\n\nOpen GL Version: ";

    // getting format information containing OpenGL version and profile
    QSurfaceFormat format = m_glWidget->format();

    // convert version number to string
    char versionBuffer[100];
    snprintf(versionBuffer, sizeof(versionBuffer), "%d.%d", format.majorVersion(), format.minorVersion());
    text.append(versionBuffer);

    // append profile information
    text.append("\nOpenGL Profile: ");
    std::string profileNames[] = {"No Profile", "Core Profile", "Compatibility Profile"};
    text.append(profileNames[format.profile()]);

    // show a message box with the OpenGL information
    QMessageBox::information(this, "About Hello GL", text.c_str());
}

void MainWindow::updateStatusBarText(QVector3D camera, QVector3D direction)
{
    // starting text
    std::string text = "Camera Position: ";

    // append camera position
    char cBuffer[100];
    snprintf(cBuffer, sizeof(cBuffer), "(x: %.2f, y: %.2f, z: %.2f) - ", static_cast<double>(camera.x()),
             static_cast<double>(camera.y()), static_cast<double>(camera.z()));
    text.append(cBuffer);

    text.append("View Direction: ");

    // append view direction
    char dBuffer[100];
    snprintf(dBuffer, sizeof(dBuffer), "(x: %.2f, y: %.2f, z: %.2f)", static_cast<double>(direction.x()),
             static_cast<double>(direction.y()), static_cast<double>(direction.z()));
    text.append(dBuffer);

    // show text in status bar
    m_statusBar->showMessage(text.c_str());
}
