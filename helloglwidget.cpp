#define TINYGLTF_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION

#include "helloglwidget.h"
#include <QMouseEvent>
#include <QOpenGLShaderProgram>
#include <QCoreApplication>
#include <math.h>
#include <iostream>

const unsigned int numTris = 2;
const unsigned int numVerts = 4;

const GLfloat vertexPosition[3 * numVerts] =
{
    -1.0, -1.0, 0.0,
    -1.0, 1.0, 0.0,
    1.0, -1.0, 0.0,
    1.0, 1.0, 0.0
};

const GLuint vertexIndex[3 * numTris] =
{
    0, 1, 2,
    1, 3, 2
};

HelloGLWidget::HelloGLWidget(TimingGraph *timingGraph, QWidget *parent)
    : QOpenGLWidget(parent), m_debugLogger(Q_NULLPTR), m_timingGraph(timingGraph)
{
    m_timer.setInterval(1000 / 30);
    connect(&m_timer, &QTimer::timeout, this, &HelloGLWidget::handleTimeout);
}

void HelloGLWidget::resetCamera()
{
    m_camera = QVector3D(2, 2, 3);
    m_center = QVector3D(0, 0, 0);
    emit cameraChanged(QVector3D(m_camera), getCameraViewDirection());
    update();
}

QVector3D HelloGLWidget::getCameraPosition()
{
    return QVector3D(m_camera);
}

QVector3D HelloGLWidget::getCameraViewDirection()
{
    return getCameraViewVector().normalized();
}

void HelloGLWidget::setLightX(int x)
{
    m_lightPos.setX(x);
    update();
}

void HelloGLWidget::setLightY(int y)
{
    m_lightPos.setY(y);
    update();
}

void HelloGLWidget::setLightZ(int z)
{
    m_lightPos.setZ(z);
    update();
}

void HelloGLWidget::setLightIntensity(int i)
{
    m_lightIntensity = map(static_cast<unsigned int>(i), 0, 10, 0, 2);
    update();
}

void HelloGLWidget::handleTimeout()
{
    update();
}

void HelloGLWidget::deferredShaderMode()
{
    m_fullImage = false;
}

void HelloGLWidget::fullImageShaderMode()
{
    m_fullImage = true;
}

void HelloGLWidget::playAnimation()
{
    m_playAnimations = true;
}

void HelloGLWidget::pauseAnimation()
{
    m_playAnimations = false;
}

void HelloGLWidget::defaultShaderMode()
{
    m_outputMode = SHADER_OUTPUT_MODE_DEFAULT;
}

void HelloGLWidget::normalShaderMode()
{
    m_outputMode = SHADER_OUTPUT_MODE_NORMAL;
}

void HelloGLWidget::textureCoordShaderMode()
{
    m_outputMode = SHADER_OUTPUT_MODE_TEXTURE_COORD;
}

void HelloGLWidget::materialIdShaderMode()
{
    m_outputMode = SHADER_OUTPUT_MODE_MATERIAL_ID;
}

void HelloGLWidget::materialColorShaderMode()
{
    m_outputMode = SHADER_OUTPUT_MODE_MATERIAL_COLOR;
}

void HelloGLWidget::reconstructedPositionShaderMode()
{
    m_outputMode = SHADER_OUTPUT_MODE_RECONSTRUCTED_POSITION;
}

void HelloGLWidget::loadModel(QString fileName)
{
    if (m_scene != nullptr)
        delete m_scene;

    m_scene = new Scene(this);
    if (m_scene->loadModel(fileName))
    {
        m_loadModel = true;
        update();
    }
}

// loader shader code from given files and link it
void linkShader(QOpenGLShaderProgram* program, QString vertex, QString fragment)
{
    program->addShaderFromSourceFile(QOpenGLShader::Vertex, vertex);
    program->addShaderFromSourceFile(QOpenGLShader::Fragment, fragment);
    program->link();
}

void HelloGLWidget::initializeGL()
{
    // init Open GL functions
    initializeOpenGLFunctions();

    m_debugLogger = new DebugLogger();
    glEnable(GL_DEBUG_OUTPUT);

    // default background color
    glClearColor(0.5F, 0.5F, 0.5F, 1.0F);

    // load and link shaders
    m_geometryShader = new QOpenGLShaderProgram();
    m_deferredShader = new QOpenGLShaderProgram();
    m_fullImageShader = new QOpenGLShaderProgram();
    linkShader(m_geometryShader, ":/geometry.vert", ":/geometry.frag");
    linkShader(m_deferredShader, ":/deferred.vert", ":/deferred.frag");
    linkShader(m_fullImageShader, ":/fullimage.vert", ":/fullimage.frag");

    // enable OpenGL depth test
    glEnable(GL_DEPTH_TEST);

    for (unsigned int i = 0; i < 3; i++)
    {
        std::vector<GLuint> vector(2);
        glGenQueries(2, vector.data());
        m_qos.push_back(std::pair<GLuint, GLuint>(vector[0], vector[1]));
    }

    // generate gBuffers, fbo
    glGenTextures(3, m_gBuffer);
    glGenFramebuffers(1, &m_fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);

    const GLenum buffers[2] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
    glDrawBuffers(2, buffers);

    glGenVertexArrays(1, &m_vao);

    glBindVertexArray(m_vao);

    glGenBuffers(1, &m_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    glBufferData(GL_ARRAY_BUFFER, 3 * numVerts * sizeof(GLfloat), vertexPosition, GL_STATIC_DRAW);
    m_deferredShader->bind();
    m_deferredShader->enableAttributeArray(4);
    m_deferredShader->setAttributeBuffer(4, GL_FLOAT, 0, 3);
    m_fullImageShader->bind();
    m_fullImageShader->enableAttributeArray(4);
    m_fullImageShader->setAttributeBuffer(4, GL_FLOAT, 0, 3);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &m_ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * numTris * sizeof(GLuint), vertexIndex, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    m_geometryShader->bind();
    m_timer.start();
}

void HelloGLWidget::paintGL()
{
    // geometry pass
    glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // load model if necessary
    if (m_loadModel)
    {
        m_loadModel = false;
        m_scene->initialize();
        m_modelLoaded = true;
    }

    animate();

    // compute view matrix
    m_v.setToIdentity();
    m_v.lookAt(m_camera, m_center, m_up);

    m_qrs.clear();
    measureTime(0);

    initTexture(m_gBuffer[0], GL_RGBA32F, GL_RGBA, GL_FLOAT, GL_COLOR_ATTACHMENT0);
    initTexture(m_gBuffer[1], GL_R8I, GL_RED_INTEGER, GL_INT, GL_COLOR_ATTACHMENT1);
    initTexture(m_gBuffer[2], GL_DEPTH_COMPONENT32F, GL_DEPTH_COMPONENT, GL_FLOAT, GL_DEPTH_ATTACHMENT);
    glBindTexture(GL_TEXTURE_2D, 0);

    m_geometryShader->bind();
    m_geometryShader->setUniformValue("v", m_v);
    m_geometryShader->setUniformValue("p", m_p);
    m_geometryShader->setUniformValue("cameraPos", m_camera);
    m_geometryShader->setUniformValue("lightPos", m_lightPos);
    m_geometryShader->setUniformValue("lightIntensity", m_lightIntensity);

    if (m_modelLoaded)
    {
        m_scene->draw(m_geometryShader);
    }

    measureTime(1);

    // deferred shading pass
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebufferObject());
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glBindVertexArray(m_vao);
    bindDeferredShader(m_fullImage ? m_fullImageShader : m_deferredShader);

    if (m_modelLoaded)
    {
        m_scene->bindMaterials(m_fullImage ? m_fullImageShader : m_deferredShader);
    }

    for (unsigned int i = 0; i < 3; i++)
    {
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D, m_gBuffer[i]);
    }

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
    glDrawElements(GL_TRIANGLES, 3 * numTris, GL_UNSIGNED_INT, nullptr);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    measureTime(2);
    reportTimings();
    m_even = !m_even;
}

void HelloGLWidget::resizeGL(int width, int height)
{
    // update viewport and projection matrix
    glViewport(0, 0, width, height);
    m_p.setToIdentity();
    m_p.perspective(45.0F, GLfloat(width) / height, 0.1F, 100.0F);
}

void HelloGLWidget::mousePressEvent(QMouseEvent *event)
{
    // check which button has been pressed and set it's flag
    if (event->button() == Qt::MouseButton::RightButton)
    {
        m_mouseOrigin = event->pos();
        m_rightMousePressed = true;
        m_leftMousePressed = false;
    }
    else if (event->button() == Qt::MouseButton::LeftButton)
    {
        m_mouseOrigin = event->pos();
        m_rightMousePressed = false;
        m_leftMousePressed = true;
    }
}

void HelloGLWidget::mouseReleaseEvent(QMouseEvent *event)
{
    // check which button hass been released
    if (event->button() == Qt::MouseButton::RightButton)
    {
        m_rightMousePressed = false;
    }
    else if (event->button() == Qt::MouseButton::LeftButton)
    {
        m_leftMousePressed = false;
    }
}

// compute delta between old and new point and update old point
QPoint getDelta(QPoint *origin, QMouseEvent *event)
{
    QPoint translation = (event->pos() - *origin);
    *origin = event->pos();
    return translation;
}

void HelloGLWidget::mouseMoveEvent(QMouseEvent *event)
{
    // check if translation is needed
    if (m_rightMousePressed)
    {
        // get delta
        QPoint delta = getDelta(&m_mouseOrigin, event);

        // horizontal translation
        QVector3D left = QVector3D::crossProduct(getCameraViewDirection(), m_up).normalized();
        m_camera -= left * delta.x() * m_mouseSpeed;
        m_center -= left * delta.x() * m_mouseSpeed;

        // vertical translation
        QVector3D down = QVector3D::crossProduct(getCameraViewDirection(), left).normalized() * delta.y() * m_mouseSpeed;
        m_camera -= down;
        m_center -= down;
        emit cameraChanged(QVector3D(m_camera), getCameraViewDirection());

        update();
    }

    // check if rotation is needed
    if (m_leftMousePressed)
    {
        // get delta
        QPoint delta = getDelta(&m_mouseOrigin, event);

        // rotation using Euler angles
        QMatrix4x4 rotation;
        rotation.rotate(-delta.x() * m_mouseSpeed * 50, 0, 1);
        rotation.rotate(-delta.y() * m_mouseSpeed * 50, 1, 0);
        m_camera = rotation * (m_camera - m_center) + m_center;
        emit cameraChanged(QVector3D(m_camera), getCameraViewDirection());

        update();
    }
}

void HelloGLWidget::wheelEvent(QWheelEvent *event)
{
    // perform zooming
    m_camera += getCameraViewDirection() * event->delta() * m_mouseSpeed;
    emit cameraChanged(QVector3D(m_camera), getCameraViewDirection());
    update();
}

QVector3D HelloGLWidget::getCameraViewVector()
{
    return m_center - m_camera;
}

float HelloGLWidget::getCameraViewDistance()
{
    return getCameraViewVector().length();
}

float HelloGLWidget::map(unsigned int i, float start1, float end1, float start2, float end2)
{
    return (i - start1) / (end1 - start1) * (end2 - start2) + start2;
}

void HelloGLWidget::measureTime(unsigned int index)
{
    std::pair<GLuint, GLuint> &pair = m_qos[index];
    glQueryCounter(m_even ? pair.first : pair.second, GL_TIMESTAMP);
    GLuint64 result;
    glGetQueryObjectui64v(m_even ? pair.second : pair.first, GL_QUERY_RESULT, &result);
    m_qrs.push_back(result);
}

void HelloGLWidget::reportTimings()
{
    std::vector<GLuint64> timings;
    if (m_qrs.size() < 2)
        return;

    for (unsigned int i = 1; i < m_qrs.size(); i++)
    {
        GLuint64 t0 = m_qrs.at(i - 1);
        GLuint64 t1 = m_qrs.at(i);
        timings.push_back(t1 - t0);
    }

    m_timingGraph->pushData(m_frameNumber++, timings);
}

void HelloGLWidget::animate()
{
    if (!m_playAnimations)
    {
        m_frameTime.restart();
        return;
    }
    else
    {
        m_animationTime += m_frameTime.restart() / 1000.0F;
        if (m_modelLoaded)
        {
            if (m_scene->animate(m_animationTime))
            {
                m_animationTime = 0;
            }
        }
    }
}

void HelloGLWidget::initTexture(GLuint id, GLint internalFormat, GLenum format, GLenum type, GLenum attachment)
{
    glBindTexture(GL_TEXTURE_2D, id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width(), height(), 0, format, type, nullptr);
    glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, id, 0);
}

void HelloGLWidget::bindDeferredShader(QOpenGLShaderProgram *shader)
{
    shader->bind();
    shader->setUniformValue("v", m_v);
    shader->setUniformValue("p", m_p);
    shader->setUniformValue("cameraPos", m_camera);
    shader->setUniformValue("lightPos", m_lightPos);
    shader->setUniformValue("lightIntensity", m_lightIntensity);
    shader->setUniformValue("pInv", m_p.inverted());
    shader->setUniformValue("mode", m_outputMode);
}
