#version 400

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;

out vec4 clipSpacePos;
out vec3 clipSpaceNormal;
out vec2 texCoord;

uniform mat4 m;
uniform mat4 v;
uniform mat4 p;
uniform mat4 n;

void main(void)
{
    gl_Position = (p * v * m) * vec4(pos, 1);
    clipSpacePos = gl_Position;
    clipSpaceNormal = normalize(mat3(n) * normal);
    texCoord = uv;
}
