#version 400

in vec4 clipSpacePos;
in vec3 clipSpaceNormal;
in vec2 texCoord;

layout(location = 0) out vec4 nuv;
layout(location = 1) out int matId;

uniform int materialId;

void main(void)
{
    vec3 normal = normalize(clipSpaceNormal);
    nuv = vec4(normal.x, normal.y, texCoord.x, texCoord.y);
    matId = materialId;
}
