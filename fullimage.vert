#version 440

layout(location = 4) in vec3 pos;

out vec2 uv;

void main(void)
{
    gl_Position = vec4(pos, 1);
    uv = (pos.xy + vec2(1)) / 2.0;
}
