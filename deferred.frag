#version 440

struct Material
{
    vec4 diffuseFactor;
    int diffuseTexture;
    float shininessFactor;
    int shininessTexture;
    int specularTexture;
    vec3 specularFactor;
};

in vec2 uv;

out vec4 frag;

uniform mat4 v;
uniform mat4 p;
uniform mat4 pInv;
uniform vec3 cameraPos;
uniform vec3 lightPos;
uniform float lightIntensity;
uniform int mode;
uniform int numberOfMaterials;

layout(binding = 0) uniform sampler2D nuvTexture;
layout(binding = 1) uniform isampler2D matTexture;
layout(binding = 2) uniform sampler2D depthTexture;
layout(binding = 3) uniform sampler2DArray textureArray;

layout(std140, binding = 4) uniform MaterialBlock
{
    Material materials[256];
};

void main(void)
{
    // reconstruct position
    float depth = texture2D(depthTexture, uv).r;
    vec3 pos = vec3(pInv * vec4(uv, depth, 1));

    // reconstruct normal
    vec2 normalWoZ = texture2D(nuvTexture, uv).xy;
    float normalZ = sqrt(1 - normalWoZ.x * normalWoZ.x - normalWoZ.y * normalWoZ.y);
    vec3 normal = normalize(vec3(normalWoZ, normalZ));

    // read texture coordinate
    vec2 texCoord = texture2D(nuvTexture, uv).zw;

    // read material data
    int materialId = texture(matTexture, uv).r;
    Material material = materials[materialId];
    vec3 k_d = clamp(vec3(material.diffuseFactor), vec3(0), vec3(1));
    vec3 k_s = clamp(material.specularFactor, vec3(0), vec3(1));
    float n = max(material.shininessFactor, 0);

    // render modes
    if (mode == 1) // normal mode
    {
        frag = vec4(normal, 1);
    }
    else if (mode == 2) // texture coord mode
    {
        frag = vec4(texCoord, 0, 1);
    }
    else if (mode == 3) // material id mode
    {
        if (numberOfMaterials == 0)
        {
            frag = vec4(0, 0, 0, 1);
        }
        else
        {
            frag = vec4(vec3(materialId * 1.0F / numberOfMaterials), 1);
        }
    }
    else if (mode == 4) // material diffuse color mode
    {
        if (material.diffuseTexture >= 0)
        {
            k_d *= vec3(texture(textureArray, vec3(texCoord, material.diffuseTexture)));
        }

        frag = vec4(k_d, 1);
    }
    else if (mode == 5) // reconstructed position
    {
        frag = vec4(pos, 1);
    }
    else // normal rendering
    {
        if (material.diffuseTexture >= 0)
        {
            k_d *= vec3(texture(textureArray, vec3(texCoord, material.diffuseTexture)));
        }

        if (material.specularTexture >= 0)
        {
            k_s *= vec3(texture(textureArray, vec3(texCoord, material.specularTexture)));
        }

        if (material.shininessTexture >= 0)
        {
            n *= length(texture(textureArray, vec3(texCoord, material.shininessTexture)));
        }

        vec3 k_a = 0.1 * k_d;
        float I_L = lightIntensity;
        vec3 V = normalize(pos - vec3(v * vec4(cameraPos, 1.0)));
        vec3 N = normalize(normal);
        vec3 L = normalize(vec3(v * vec4(lightPos, 1.0)) - pos);
        vec3 R = normalize(reflect(L, N));
        frag = vec4(k_a * I_L + k_d * I_L * max(0, dot(N, L)) + k_s * I_L * pow(max(0, dot(R, V)), n), 0);
    }
}
