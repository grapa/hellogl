#ifndef DEBUGLOGGER_H
#define DEBUGLOGGER_H

#include <QWidget>
#include <QOpenGLFunctions_4_4_Core>
#include <QOpenGLDebugMessage>
#include <QOpenGLDebugLogger>

class DebugLogger : public QWidget, protected QOpenGLFunctions_4_4_Core
{
public:
    DebugLogger();
    ~DebugLogger();

public slots:
    void messageLogged(const QOpenGLDebugMessage &msg);

private:
    QOpenGLDebugLogger *m_debugLogger = nullptr;
};

#endif // DEBUGLOGGER_H
