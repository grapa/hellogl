#ifndef HELLOGLWIDGET_H
#define HELLOGLWIDGET_H

#include "scene.h"
#include "debuglogger.h"
#include "errorchecker.h"
#include "timinggraph.h"
#include <QOpenGLWidget>
#include <QOpenGLFunctions_4_4_Core>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLDebugMessage>
#include <QOpenGLDebugLogger>

// custom OpenGL widget
class HelloGLWidget : public QOpenGLWidget, protected ErrorChecker
{
    // necessary macro to enable Qt functionalities like signals, slots, etc.
    Q_OBJECT

public:
    // derived constructor
    HelloGLWidget(TimingGraph *timingGraph, QWidget *parent = 0);
    // derived destructor (default so we don't have to implement it)
    virtual ~HelloGLWidget() = default;

    // resets the camera and zoom to it's default position
    void resetCamera();

    // returns the camera position
    QVector3D getCameraPosition();
    // returns the normalized camera view direction
    QVector3D getCameraViewDirection();

    // loads a glTF model from the given file
    void loadModel(QString fileName);

public slots:
    // sets the x coordinate of the light position to the given value
    void setLightX(int x);
    // sets the y coordinate of the light position to the given value
    void setLightY(int y);
    // sets the z coordinate of the light position to the given value
    void setLightZ(int z);
    // sets the light intensity to the given value
    void setLightIntensity(int i);
    void handleTimeout();
    void deferredShaderMode();
    void fullImageShaderMode();
    void playAnimation();
    void pauseAnimation();
    void defaultShaderMode();
    void normalShaderMode();
    void textureCoordShaderMode();
    void materialIdShaderMode();
    void materialColorShaderMode();
    void reconstructedPositionShaderMode();

signals:
    // raises a signal when the camera changes to the given parameters
    void cameraChanged(QVector3D camera, QVector3D direction);

protected:
    // initializes OpenGL
    void initializeGL() override;
    // paints the scene
    void paintGL() override;
    // triggered when the widget size changes
    // resizes the drawing area
    void resizeGL(int width, int height) override;
    // triggered when the user presses a mouse button
    void mousePressEvent(QMouseEvent *event) override;
    // triggered when the user releases a mouse button
    void mouseReleaseEvent(QMouseEvent *event) override;
    // triggered when the user moves the mouse
    void mouseMoveEvent(QMouseEvent *event) override;
    // triggered when the user uses the scroll wheel
    void wheelEvent(QWheelEvent *event) override;

private:
    void animate();
    // returns the vector from the camera to it's lookat point
    QVector3D getCameraViewVector();
    // returns the cameras distance to it's lookat point
    float getCameraViewDistance();
    float map(unsigned int i, float start1, float end1, float start2, float end2);
    void measureTime(unsigned int index);
    void reportTimings();
    void initTexture(GLuint id, GLint internalFormat, GLenum format, GLenum type, GLenum attachment);
    void bindDeferredShader(QOpenGLShaderProgram *shader);

    QOpenGLShaderProgram *m_geometryShader;
    QOpenGLShaderProgram *m_deferredShader;
    QOpenGLShaderProgram *m_fullImageShader;

    // the view matrx
    QMatrix4x4 m_v;
    // the projection matrix
    QMatrix4x4 m_p;
    // the camera position
    QVector3D m_camera = QVector3D(2, 2, 3);
    // the point the camera looks at
    QVector3D m_center = QVector3D(0, 0, 0);
    // the up vector of the camera (here y axis)
    QVector3D m_up = QVector3D(0, 1, 0);
    // the light position in world coordinates
    QVector3D m_lightPos = QVector3D(-2, 2, -1);
    // the light intensity
    float m_lightIntensity = 1.0F;
    // the last point of a mouse event
    QPoint m_mouseOrigin;
    // whether the right mouse button is currently pressed
    bool m_rightMousePressed;
    // whether the left mouse button is currently pressed
    bool m_leftMousePressed;
    // a multiplier to slow down zoom, translation and rotation
    float m_mouseSpeed = 0.002F;
    // the current shininess (Phong exponent)
    int m_shininess = 1;
    // the current shader mode (0 = wireframe, 1 = Gouraud, 2 = Phong)
    int m_shaderMode = 2;
    // whether a glTF model should be loaded in the next frame
    bool m_loadModel = false;
    // whether a glTF model has been loaded (disables tessellation)
    bool m_modelLoaded = false;
    DebugLogger *m_debugLogger = nullptr;
    Scene *m_scene = nullptr;
    TimingGraph *m_timingGraph;
    QTimer m_timer;
    bool m_playAnimations = true;
    ShaderOutputMode m_outputMode = 0;
    bool m_fullImage = false;

    std::vector<std::pair<GLuint, GLuint>> m_qos;
    std::vector<GLuint64> m_qrs;
    QTime m_frameTime;
    float m_animationTime = 0;
    int m_frameNumber = 0;
    bool m_even = true;

    GLuint m_gBuffer[3];
    GLuint m_fbo;
    GLuint m_vao;
    GLuint m_vbo;
    GLuint m_ibo;
};

#endif // HELLOGLWIDGET_H
