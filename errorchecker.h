#ifndef ERRORCHECKER_H
#define ERRORCHECKER_H

#include <QOpenGLFunctions_4_4_Core>

class ErrorChecker : protected QOpenGLFunctions_4_4_Core
{
public:
    ErrorChecker();

    void checkGlError(const char* prefix);
};

#endif // ERRORCHECKER_H
